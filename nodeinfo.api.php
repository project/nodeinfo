<?php

/**
 * @file
 * Hooks specific to the NodeInfo module.
 */

/**
 * Alter the data which is send in the Nodeinfo callback.
 *
 * @param $data
 */
function hook_nodeinfo_alter(&$data) {
  $data['openRegistrations'] = TRUE;
}
