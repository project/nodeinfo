<?php

namespace Drupal\nodeinfo\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class NodeInfoController extends ControllerBase {

  /**
   * The Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The ModuleHandler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The ModuleExtensionList service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * NodeInfoController constructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   */
  public function __construct(RendererInterface $renderer, ModuleHandlerInterface $moduleHandler, ModuleExtensionList $moduleExtensionList) {
    $this->renderer = $renderer;
    $this->moduleHandler = $moduleHandler;
    $this->moduleExtensionList = $moduleExtensionList;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer'),
      $container->get('module_handler'),
      $container->get('extension.list.module')
    );
  }

  /**
   * Returns the NodeInfo link elements.
   *
   * @return \Drupal\Core\Cache\CacheableJsonResponse
   */
  public function nodeInfo() {

    $context = new RenderContext();
    $url = $this->renderer->executeInRenderContext($context, function () {
      return Url::fromRoute('nodeinfo.content', [], ['absolute' => TRUE])->toString();
    });

    $data = [
      'links' => [
        [
          'href' => $url,
          'rel' => 'http://nodeinfo.diaspora.software/ns/schema/2.0',
        ]
      ],
    ];
    return new CacheableJsonResponse($data);
  }

  /**
   * Returns the NodeInfo content.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function content() {
    $data = [
      'version' => "2.0",
      'openRegistrations' => $this->config('user.settings')->get('register') != UserInterface::REGISTER_ADMINISTRATORS_ONLY,
      'software' => [
        'name' => 'drupal',
        'version' => $this->moduleExtensionList->getExtensionInfo('nodeinfo')['version'],
      ],
      'services' => [
        'inbound' => [],
        'outbound' => [],
      ],
    ];
    $this->moduleHandler->alter('nodeinfo', $data);
    return new JsonResponse($data);
  }

}
